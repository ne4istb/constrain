// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.violation;

import 'package:matcher/matcher.dart';
import 'package:constrain/metadata.dart';
import 'preconditions.dart';
import 'package:option/option.dart';

/**
 * Captures the details of a [Constraint] that has been violated
 */
class ConstraintViolation<T> {
  final ConstraintDescriptor constraintDescriptor;
  final T rootObject;
  final Object leafObject;
  final Object invalidValue;
  final Iterable<Symbol> propertyPath;
  final String propertyPathString;
  String _message;
  String reason; // TODO: do we need this and constraintDescriptor.constraint.description
  final Option<ViolationDetails> details;

  Type get _rootType => rootObject == null ? null : rootObject.runtimeType;

  String get message {
    if (_message == null) {
      String locationDesc = 'Constraint violated at path $propertyPathString';
      // TODO: build up reason from non ensures
      // TODO: build proper message
      final String description = constraintDescriptor.constraint.description !=
          null ? '${constraintDescriptor.constraint.description}\n' : '';
      final _reason = reason != null ? reason : '';
      final _details = details.map((d) => '$d\n').getOrElse(() => '');
      _message = '$locationDesc\n$description$_reason$_details';
    }
    return _message;
  }

  String get verboseMessage {
    String objectDesc =
        _rootType != null ? 'Invalid $_rootType ($rootObject)' : '';
    return '$objectDesc. $message';
  }

  ConstraintViolation(
      this.constraintDescriptor,
      this.invalidValue,
      this.leafObject,
      this.propertyPath,
      this.propertyPathString,
      this.rootObject,
      {String message,
      this.reason,
      this.details: const None()})
      : this._message = message {
    ensure(constraintDescriptor, isNotNull);
    ensure(leafObject, isNotNull);
    ensure(leafObject, isNotNull);
    ensure(propertyPath, isNotNull); // TODO: is empty valid
    ensure(rootObject, isNotNull);
  }

  String toString() => message;

  Map toJson() => {
        'constraint': constraintDescriptor.constraint.toJson(),
        'message': message,
        'rootObject': _objectToJson(rootObject),
        'leafObject': _objectToJson(leafObject),
        'invalidValue': _objectToJson(invalidValue),
        'propertyPath': propertyPathString,
        'details': details.map((d) => d.toJson()).getOrElse(() => null),
        'reason': reason
      };

  Map _objectToJson(object) => {
        'type': (object == null ? 'unknown' : object.runtimeType.toString()),
        'value': object
      };
}

class ViolationDetails {
  final String expected;
  final String actual;
  final String mismatchDescription;

  ViolationDetails(this.expected, this.actual, this.mismatchDescription) {
    ensure(expected, isNotNull);
    ensure(actual, isNotNull);
  }

  String toString() {
    final description = new StringBuffer();
    description..write('Expected: ')..write(expected)..write('\n');
    description..write('  Actual: ')..write(actual);
    if (mismatchDescription != null && !mismatchDescription.trim().isEmpty) {
      description
        ..write('\n')
        ..write('   Which: ')
        ..write(mismatchDescription.toString());
    }
    return description.toString();
  }

  Map toJson() => {
        'expected': expected,
        'actual': actual,
        'mismatchDescription': mismatchDescription
      };
}

class ConstraintViolationException {
  final Set<ConstraintViolation> violations;

  ConstraintViolationException(this.violations);

  String toString() => '$violations';
}
