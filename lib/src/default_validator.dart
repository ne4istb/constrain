// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.validate.impl;

import '../constraint.dart';
import '../validator.dart';
import 'package:constrain/metadata.dart';
import 'preconditions.dart';
import 'default_validator_context.dart';

final Set _noViolations = new Set<ConstraintViolation>();

class DefaultValidator implements Validator {
  final TypeDescriptorResolver resolver;

  DefaultValidator(this.resolver) {
    ensure(resolver, isNotNull);
  }

  Set<ConstraintViolation> validate(dynamic object,
      {Iterable<ConstraintGroup> groups}) {
    if (object == null) {
      return _noViolations;
    }

    final context = new TypeValidationContext.root(object, resolver,
        group: groups != null ? new CompositeGroup(groups) : null);
    context.validate();
    return context.build();
  }

  @override
  Set<ConstraintViolation> validateFunctionParameters(
      Function function, List positionalParameters,
      {Map<Symbol, dynamic> namedParameters,
      Iterable<ConstraintGroup> groups}) {
    if (function == null) {
      return _noViolations;
    }

    final context = new FunctionParameterValidationContext(
        function, positionalParameters, namedParameters, resolver,
        group: groups != null ? new CompositeGroup(groups) : null);
    context.validate();
    return context.build();
  }

  @override
  Set<ConstraintViolation> validateFunctionReturn(
      Function function, returnValue,
      {Iterable<ConstraintGroup> groups}) {
    if (function == null) {
      return _noViolations;
    }

    final context = new FunctionReturnValidationContext(
        function, returnValue, resolver,
        group: groups != null ? new CompositeGroup(groups) : null);
    context.validate();
    return context.build();
  }
}
