// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.core_constraints.pattern.test;

import 'package:constrain/src/core_constraints/pattern.dart';
import 'package:constrain/constraint.dart';
import 'package:constrain/metadata.dart';
import 'package:test/test.dart';

import 'dart:core' as core show Pattern;
import 'dart:core' hide Pattern;

main() {
  group('Pattern', () {
    test('produces correct description', () {
      expect(const Pattern('somestring').description,
          equals('must match "somestring"'));
    });

    test('allows description to be overridden', () {
      expect(
          const Pattern('somestring', description: 'overriden description')
              .description,
          equals('overriden description'));
    });

    group('validate', () {
      group('returns empty set', () {
        final pattern = const Pattern(r'[\w]+-[\d]+');

        Set<ConstraintViolation> validate(String value) {
          return _validate(pattern, value);
        }

        test('when value matches pattern', () {
          expect(validate('ABC-123'), isEmpty);
        });
      });

      group('returns constraint violations', () {
        final pattern = const Pattern(r'[\w]+-[\d]+');

        Set<ConstraintViolation> validate(String value) {
          return _validate(pattern, value);
        }

        test('when value doesnt matches pattern', () {
          expect(validate('ABC'), isNot(isEmpty));
        });
      });
    });
  });
}

class TestConstraintValidationContext extends ConstraintValidationContext {
  Constraint constraint;
  dynamic owner;
  Set<ConstraintViolation> violations = new Set();

  void addViolation({String reason, ViolationDetails details}) {
    violations.add(new ConstraintViolation(new ConstraintDescriptor(constraint),
        "null", "null", [], "null", "null"));
  }
}

Set<ConstraintViolation> _validate(Constraint constraint, value) {
  var context = new TestConstraintValidationContext()..constraint = constraint;
  constraint.validate(value, context);
  return context.violations;
}
