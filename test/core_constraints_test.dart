// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.core_constraints.test;

import 'package:test/test.dart';
import 'core_constraints/min_max_test.dart' as min_max;
import 'core_constraints/pattern_test.dart' as pattern;

main() {
  group('[min_max]', min_max.main);
  group('[pattern]', pattern.main);
}
